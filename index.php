<?php
include "class/func.php";
include "elements/redirect/redirect.php";
$get_mod = filter_input(INPUT_GET, 'mod', FILTER_SANITIZE_STRING);
$get_api = filter_input(INPUT_GET, 'api', FILTER_SANITIZE_NUMBER_INT);
$file = "modules/" . $get_mod . "/" . $get_mod . ".php";
if ($get_mod == "" || !file_exists($file)){
  $get_mod='main';
}
$title = array(
    "main" => "Главная",
    "works" => "Проекты",
    "about" => "О нас",
    "contacts" => "Контакты",
    "articles" => "Читай подробнее в нашей статье",
    "what" => "Информация"
);
$dest = array(
    "main" => "Создание сайтов, разработка мобильных приложений и компьютерных программ  для бизнеса в Архангельске, Северодвинске и области.",
    "works" => "Здесь представлены проекты работ нашей команды. Список очень скоро пополнится - мы без заказов не сидим.",
    "about" => "Читай о нас, присоединяйся к нам ВКонтакте - там свежие новости, новые предложения и интересная информация. Разработка29 - никогда не знаешь, когда понадобятся программисты",
    "contacts" => "Заказов много не бывает, поэтому звоните и пишите. Пообщаемся и станет понятно, насколько плодотворным станет наше сотрудничество.",
    "articles" => "Читай, почему наши услуги влекут увеличение дохода.",
    "what" => "Справка, объясняющая что к чему."
);
$keywords = array(
    "main" => "создание сайтов в архангельске,создание мобильных приложений в архангельске, мобильные приложения архангельск,создание программ архангельск, купить по архангельск,
услуги программистов архангельск, разработчики программ архангельск,создание сайтов в северодвинске,создание мобильных приложений в северодвинске,
 мобильные приложения северодвинск,создание программ северодвинск, купить по северодвинск,услуги программистов северодвинск, 
 разработчики программ северодвинск",
    "works" => "электронная цикловая коммиссия архангельск, база клиентов архангельск, база заказов архангельск, сайт для организации Архангельск,
электронная цикловая коммиссия северодвинск, база клиентов северодвинск, база заказов северодвинск, сайт для организации северодвинск,
мобильное приложение Султан Суши,",
    "about" => "Грищенков Никита, Шемелин Алексей, Прибытков Никита, Грищенков Никита архангельск, Шемелин Алексей архангельск, Прибытков Никита архангельск,
 выпускники АКТ, 	Архангельский колледж телекоммуникаций выпускники, программирование в компьютерных системах, 
программисты в Архангельске, разработкчики архангельск, программист архангельск, вакансия программиста архангельск,
программисты в северодвинске, разработкчики северодвинск, программист северодвинск, вакансия программиста северодвинск",
    "contacts" => "номер программиста в Архангельске, 79021947450, developer29@yandex.ru, Разработка29, оставить заявку программисту, 
	контакты архангельск, заказать разработку архангельск, номер программиста в северодвинске, контакты программиста северодвинск, заказать разработку северодвинск",
    "articles" => "Клиентская база Архангельск, создать базу данных в Архангельске, мобильное приложение в Северодвинске,
	мобильное приложение в Архангельске, напоминалка для огранизации, система управления заданиями, 
	сократить расходы, компания по разработке программного обеспечения",
    "what" => "что такое программное обеспечение, что такое ПО, что такое сайт, что такое веб-сайт, Архангельск, Северодвинск, Новодвинск, 
	эксклюзивный дизайн сайта в Архангельске, ПО с базой данных"
);
$str = scripts();
if (($get_api === "1") || ($post_api === "1")) {
	if (isset($_REQUEST[ "mod" ]) && $_REQUEST[ "mod" ] != "") {
		require_once "modules/" . $_REQUEST[ "mod" ] . "/api." . $_REQUEST[ "mod" ] . ".php";
	}
	exit();
}
?>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name='wmail-verification' content='666322aa57f7a7fb9c9c6f32fb2df23a' />
      <meta name="yandex-verification" content="07ebe78fa95fb1ea" />
      		<?php
      echo " <meta name=\"title\" content=\"Разработка29 - ".$title[$get_mod]."\" />";
  ?>
            		<?php
      echo " <meta name=\"description\" content=\"".$dest[$get_mod]."\" />";
  ?>      		<?php
      echo " <meta name=\"Keywords\" content=\"".$keywords[$get_mod]."\" />";
  ?>
      <META NAME="Услуги программистов в Архангельске" CONTENT="Сайт, мобильное приложение, компьютерная программа для Вас или вашего бизнеса"/>
		<?php
      echo "<title>".$title[$get_mod]."</title>";
  ?>
      	<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.разработка29.рф/">
       	<link rel="image_src" href="http://xn--29-6kcaaf4b0a9apc1a.xn--p1ai/images/logogo.png" />
      	<link rel="shortcut icon" href="/images/logo.ico" type="image/x-icon">
		<link href="css/styleone.css" rel="stylesheet" type="text/css">
      	<link href="css/fotorama.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-2.2.2.min.js"></script>
		<script type="text/javascript" src="js/fotorama.js"></script>
		<script type="text/javascript" src="js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/swiper.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/text.js" ></script>
        <script type="text/javascript" src="js/FixedHeader.js"></script>
        <script type="text/javascript" src="js/myScripts.js"></script>
      	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
 		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
		<script src="js/underscore-min.js" type="text/javascript"></script>
      	<script src="https://vk.com/js/api/openapi.js?136" type="text/javascript"></script>
      	<script type="text/javascript" src="https://vk.com/js/api/share.js?94" charset="utf-8"></script>
		<?php echo $str ?>
	</head>
	<body>
      <div id="father">
		<div id="site">
			<?php
			ShowElement("header");
			ShowElement("lockjava");
			ShowElement("module");
			?>
		</div>
		<?php
		ShowElement("offer");
		echo "</div>";
		ShowElement("footer");

		?>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96750712-1', 'auto');
  ga('send', 'pageview');

</script>
        <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43943354 = new Ya.Metrika({
                    id:43943354,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43943354" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	</body>
</html>